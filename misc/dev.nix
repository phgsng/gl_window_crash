{ pkgs ? import <nixpkgs> { } } :

pkgs.mkShell {

  nativeBuildInputs = with pkgs; [
    xorg.xorgserver
    pkgconfig
    libGL
    libGL_driver
    mesa
    xorg.libXft
    xorg.libX11
    xorg.libXcursor
    xorg.libXext
    xorg.libXft
    xorg.libXi
    xorg.libXrender
    xorg.libXt
    xorg.libXrandr
    xorg.xorgproto
    libglvnd
  ];

  buildInputs = with pkgs; [
  ];

  shellHook = ''
    export LD_LIBRARY_PATH="${pkgs.libglvnd.out}/lib"
    echo $LD_LIBRARY_PATH
  '';
}


