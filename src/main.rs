use glutin::{self,
             dpi::{PhysicalPosition, PhysicalSize},
             event_loop::EventLoopWindowTarget,
             platform::unix::{EventLoopWindowTargetExtUnix,
                              WindowBuilderExtUnix, WindowExtUnix},
             window::{CursorIcon, Fullscreen, UserAttentionType,
                      Window as GlutinWindow, WindowBuilder, WindowId},
             ContextBuilder, PossiblyCurrent, WindowedContext};
use std::fmt::{self, Display, Formatter};

mod gl {
    #![allow(clippy::all)]
    include!(concat!(env!("OUT_DIR"), "/gl_bindings.rs"));
}

#[derive(Debug)]
pub enum Error
{
    /// Error creating the window.
    ContextCreation(glutin::CreationError),

    /// Error dealing with fonts.
    //Font(crossfont::Error),

    /// Error manipulating the rendering context.
    Context(glutin::ContextError),
}

impl From<glutin::CreationError> for Error
{
    fn from(val: glutin::CreationError) -> Self { Error::ContextCreation(val) }
}

impl From<glutin::ContextError> for Error
{
    fn from(val: glutin::ContextError) -> Self { Error::Context(val) }
}
impl std::error::Error for Error
{
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)>
    {
        match self {
            Error::ContextCreation(err) => err.source(),
            Error::Context(err) => err.source(),
            //Error::Font(err) => err.source(),
        }
    }
}

impl Display for Error
{
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result
    {
        match self {
            Error::ContextCreation(err) =>
                write!(f, "Error creating GL context; {}", err),
            Error::Context(err) =>
                write!(f, "Error operating on render context; {}", err),
            //Error::Font(err) => err.fmt(f),
        }
    }
}
type Result<T> = std::result::Result<T, Error>;

fn create_gl_window<E>(
    mut window: WindowBuilder,
    event_loop: &EventLoopWindowTarget<E>,
    srgb: bool,
    vsync: bool,
    dimensions: Option<PhysicalSize<u32>>,
) -> Result<WindowedContext<PossiblyCurrent>>
{
    if let Some(dimensions) = dimensions {
        window = window.with_inner_size(dimensions);
    }

    let windowed_context = ContextBuilder::new()
        .with_srgb(srgb)
        .with_vsync(vsync)
        .with_hardware_acceleration(None)
        .build_windowed(window, event_loop)?;

    // Make the context current so OpenGL operations can run.
    let windowed_context =
        unsafe { windowed_context.make_current().map_err(|(_, err)| err)? };

    Ok(windowed_context)
}

fn main()
{
    let event_loop = glutin::event_loop::EventLoop::new();
    let window_builder = WindowBuilder::new()
        .with_title("wtf")
        .with_visible(false)
        .with_transparent(true)
        //.with_decorations(Decorations::None)
        .with_maximized(true)
        .with_fullscreen(None)
        .with_class(String::from("wtf"), String::from("omg"));

    let windowed_context =
        create_gl_window(window_builder, &event_loop, true, true, None)
            .expect("wtf");

    eprintln!("lock and load!");
    gl::load_with(|symbol| windowed_context.get_proc_address(symbol) as *const _);

    eprintln!("loaded and locked!");
}
